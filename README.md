# BIOSTAT823 Module2 Fall2023

## Render rmarkdown note books from R prompt

```{r}
> rmarkdown::render('test.Rmd')

```

## Render rmarkdown note books from shell

```{bash}
$ Rscript -e "rmarkdown::render('test.Rmd')"

```
