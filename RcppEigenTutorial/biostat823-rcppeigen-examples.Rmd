---
title: "RcppEigen Examples"
#subtitle: "BIOSTAT 823"
author: "BIOSTAT 823"
output:
  html_document:
   toc: TRUE
   theme: simplex
   code_folding: show
   highlight: tango
---

# Synopsis

A review of the following RcppEigen code snippets should be helpful
with respect to completing HW3 and HW4. 
You can use the approach
outlined in Problem 0 of HW3 to render this rmarkdown report on the
DCC cluster (you may want to increase memory allocation in the SLURM script
to 4-8GB) or alternatively render it on your own machine by
installing requisite R packages and the development toolchain. These
examples are meant to help you with basic usage of RcppEigen. For a
more advanced introduction, see the paper by Bates and Eddelbuettel
(\textit{Journal of Statistical Software} 2013) along with examples in
the RcppGallery (https://gallery.rcpp.org/).  It should be noted that
Eigen 3.4 has introduced a number of new features. Unfortunately, as
of now RcppEigen is still at 3.3. Furthermore, when programming with
RcppEigen, you should become familiar with the concepts of shallow
versus deep copying, and aliasing (see Rcpp and Eigen documentation).

# Load packages

```{r pkgs}
library(RcppEigen)
library(microbenchmark)
library(tidyverse)
library(foreach)
library(kableExtra)
```

# Add two and multiply two matrices

```{Rcpp cppfun1}
#include <Rcpp.h>
#include <RcppEigen.h>

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]
Eigen::MatrixXd addmat(Eigen::MatrixXd matA, Eigen::MatrixXd matB) {
  const Eigen::MatrixXd outmat =  matA + matB;
  return outmat;
}

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]
Eigen::MatrixXd prodmat(Eigen::MatrixXd matA, Eigen::MatrixXd matB) {
  Eigen::MatrixXd outmat =  matA * matB;
  return outmat;
}


```{r test1}
set.seed(121)
n <- 5
m <- 3
# Simulate matrices
matA <- matrix(rnorm(n*n), n, n)
matB <- matrix(rnorm(n*n), n, n)
matC <- matrix(rnorm(n*m), n, m) 

# Get sum and products
matadd <- addmat(matA, matB)
matprod <- prodmat(matA, matC)

# Print individual matrices
matA
matB
matC

# Print sum and difference 
matadd
matprod

# Check results against those from using native R operators
all.equal(matadd, matA + matB)
all.equal(matprod, matA %*% matB)

```

# Return sum and differences of two matrices as a list (version 1)

```{Rcpp cppfun2}
#include <Rcpp.h>
#include <RcppEigen.h>

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]
Rcpp::List adddiffmat(Eigen::MatrixXd matA, Eigen::MatrixXd matB) {
  const Eigen::MatrixXd matC =  matA + matB;
  const Eigen::MatrixXd matD =  matA - matB;
  return Rcpp::List::create(Rcpp::Named("add") = matC, Rcpp::Named("diff") = matD);
}

```

```{r test2}
set.seed(121)
n <- 5
# Simulate matrices
matA <- matrix(rnorm(n*n), n, n)
matB <- matrix(rnorm(n*n), n, n)
# Get sum and difference
res <- adddiffmat(matA, matB)
res
# Check results against those from using native R operators
all.equal(res[["add"]], matA + matB)
all.equal(res[["diff"]], matA - matB)

```

# Add two and multiply two matrices and return results as a list (version 2)



```{Rcpp cppfun3}
#include <Rcpp.h>
#include <RcppEigen.h>
 
using Eigen::MatrixXd;
using Rcpp::List;
using Rcpp::Named;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]
List addprodmat(MatrixXd matA, MatrixXd matB) {
  const MatrixXd matC =  matA + matB;
  const MatrixXd matD =  matA - matB;
  return List::create(Named("add") = matC, Named("diff") = matD);
}

```


```{r test3}
set.seed(121)
n <- 5
matA <- matrix(rnorm(n*n), n, n)
matB <- matrix(rnorm(n*n), n, n)
res <- addprodmat(matA, matB)
res
all.equal(res[["add"]], matA + matB)
all.equal(res[["diff"]], matA - matB)

```

# Special initializations

```{Rcpp specmat}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Rcpp::List;
using Rcpp::Named;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

List specialmatvec(int nr, int nc, double myconstant) {
  return List::create(
          Named("idmat") = MatrixXd::Identity(nr, nr), 
		  Named("onevec") = VectorXd::Ones(nr),
		  Named("onemat") = MatrixXd::Ones(nr, nc),
		  Named("zerovec") = VectorXd::Zero(nr),
		  Named("zeromat") = MatrixXd::Zero(nr, nc),
		  Named("constvec") = VectorXd::Constant(nr, myconstant),
		  Named("constmat") = MatrixXd::Constant(nr, nc, myconstant)
		  );
}

```

```{r specr}
specialmatvec(4, 3, 0.823)

```




# Augment a matrix with a vector

In this example, we will add a vector of ones to 

```{Rcpp cppaug}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

MatrixXd augmat(MatrixXd matA) {
  int nr = matA.rows();
  int nc = matA.cols();
  // Augment matrix matA with a vector of ones
  // Note that the resulting matrix is nr x (nc +1)
  MatrixXd out(nr, nc + 1);
  out << VectorXd::Ones(nr), matA;
  return out;
}

```

```{r raug}
set.seed(1212)
n <- 5
m <- 3
matA <- matrix(rnorm(n*m), n, m)
matA
augmat(matA)

```
# Computing $\mathbf{X}^T\mathbf{X}$

Given an $n \times m$ matrix $\mathbf{X}$ a straightforward approach for
computing the $m \times m$ matrix $\mathbf{X}^TX$ is

```{Rcpp trans1}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

MatrixXd XtX1 (MatrixXd X) {
  return X.transpose()*X;
}

```

Another approach outlined in the RcppEigen paper is to use


```{Rcpp trans2}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::Lower;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

MatrixXd XtX2(MatrixXd X) {
  int n(X.cols());
  return  MatrixXd(n,n).setZero().selfadjointView<Lower>().rankUpdate(X.adjoint());
}

```

Testing both approaches  on a toy matrix we get

```{r testtrans}
set.seed(32113)
n <- 5
m <- 3
X <- matrix(rnorm(n*m),n,m)
XtX1(X)
XtX2(X)
t(X) %*% X


```

Why would the second approach be preferred for practical applications despite being
more syntactically complex?

# Multiply a matrix with a vector and add random noise

```{Rcpp multnoise}

#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::Map;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]


VectorXd addnoise(MatrixXd matA, VectorXd vecx, double mu, double sigma) {
    // This function with return matA vecx + veceps where
	// veceps is a noise vector
	
    // Get number of rows (use mat.cols() to get number of columns)
	int nr = matA.rows();
	// The output vector will be of length nr
	VectorXd out(nr);

    // Simulate a nr Normal(mu, sigma) variates and save to a vector
	VectorXd veceps(nr);
	veceps = Rcpp::as< Map<VectorXd> >(Rcpp::rnorm(nr, mu,sigma));

	// Output vector 
	out = matA * vecx + veceps;
	return(out);
}
	
```

```{r noisevec}

set.seed(121)
n <- 5
m <- 3
matA <- matrix(rnorm(n*m), n, m)
vecx <- matrix(runif(m, 0, 1), m, 1)

# get vector
out <- addnoise(matA, vecx, 0, 1)

# Why does this not yield a reproducible simulation?
set.seed(121)
identical(out, addnoise(matA, vecx, 0, 1))

# Make it reproducible (Approach 1)
set.seed(121)
matA <- matrix(rnorm(n*m), n, m)
vecx <- matrix(runif(m, 0, 1), m, 1)
bootseed <- .Random.seed
out1 <- addnoise(matA, vecx, 0, 1)
out2 <- addnoise(matA, vecx, 0, 1)

## Now reproduce out1 and out2
.Random.seed <- bootseed
identical(out1, addnoise(matA, vecx, 0, 1))
identical(out2, addnoise(matA, vecx, 0, 1))

# Make it reproducible (Approach 2)
set.seed(121)
matA <- matrix(rnorm(n*m), n, m)
vecx <- matrix(runif(m, 0, 1), m, 1)
set.seed(81729)
out1 <- addnoise(matA, vecx, 0, 1)
out2 <- addnoise(matA, vecx, 0, 1)

## Now reproduce out1 and out2
set.seed(81729)
identical(out1, addnoise(matA, vecx, 0, 1))
identical(out2, addnoise(matA, vecx, 0, 1))

# You can also use either approach to reproduce natively in R
set.seed(81729)
identical(out1, as.numeric(matA%*%vecx + rnorm(n, 0, 1)))
identical(out2, as.numeric(matA%*%vecx + rnorm(n, 0, 1)))
```


# Get matrix norms

```{Rcpp cppfun4}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::Infinity;
using Rcpp::List;
using Rcpp::Named;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

List getnorms(MatrixXd matA) {
  // L-infinity (max norm)
  const double maxnorm = matA.lpNorm<Infinity>();
  // Frobenius norm
  const double sqrnorm = matA.norm();
  return List::create(Named("maxnorm") = maxnorm, Named("sqrnorm") = sqrnorm);
}

```

```{r test4}
set.seed(121)
n <- 5
matA <- matrix(rnorm(n*n), n, n)
matA
res <- getnorms(matA)

all.equal(res[["maxnorm"]], max(abs(matA)))
all.equal(res[["sqrnorm"]], sqrt(sum(matA^2)))

```

# Solve linear equation using Choleski and SVD

```{Rcpp cppfun5}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

VectorXd cholsol(MatrixXd matA, VectorXd vecb) {
  // Solve Ax = b in x using the Choleski decomposition
  // Note that you matA will need to be symmetric and pd
  const VectorXd vecx(matA.llt().solve(vecb));
  return vecx;
}
//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

VectorXd svdsol(MatrixXd matA, VectorXd vecb) {
  // Solve Ax = b in x using one of Eigen's SVD routines
  const VectorXd vecx(matA.bdcSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(vecb));
  return vecx;
}
```

```{r test5}
set.seed(121)
m <- 4
matX <- matrix(rnorm(m*m), m, m)
matA <- t(matX) %*% matX 
vecx <- rnorm(m)
vecb <- as.numeric(matA%*%vecx)
vecx

# solution using Choleski decomposition
xchol <- cholsol(matA,vecb)
xchol
# solution using SVD decomposition
xsvd <- svdsol(matA,vecb)
xsvd
all.equal(xchol, vecx)
all.equal(xsvd, vecx)


```

# Calculate numerical matrix inverse

```{Rcpp inverse}

#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::Map;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]



MatrixXd eigeninv(MatrixXd matA) {
  // Use Eigen LU based inversion
  const MatrixXd matinv(matA.inverse());
  return matinv;
}



//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

MatrixXd cholinv(MatrixXd matA) {
  // Use Choleski decomposition by solving an equation
  int nr = matA.rows();
  const MatrixXd matinv(matA.llt().solve(MatrixXd::Identity(nr, nr)));
  return matinv;
}
```

```{r rinv}
set.seed(1213)
n <- 5
X <- matrix(rnorm(n*n), n, n)
A <- t(X) %*% X
dim(A)
eigeninv(A)
# Is some caution required before applying the Choleski
# the Choleski decomposition to this symmetric matrix
cholinv(A)
eigeninv(A)%*%A
max(abs(eigeninv(A)%*%A)-diag(n))
cholinv(A)%*%A
max(abs(cholinv(A)%*%A)-diag(n))

```

# Simulate random matrix from within Eigen

```{Rcpp cppfun6}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::Map;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

MatrixXd simmat(int nrow, int ncol, double mu, double sigma) {
  // Simulate a random matrix of dimension nrow  x ncol whose
  // entries are drawn independently from a Normal(mu, sigma) law
  MatrixXd matA(nrow, ncol);
  VectorXd randvec(nrow*ncol);
  // Starting with Eigen 3.4 there is a more covenient approach to reshape
  randvec = Rcpp::as< Map<VectorXd> >(Rcpp::rnorm(nrow*ncol, mu,sigma));
  matA = Map<MatrixXd>(randvec.data(), nrow, ncol);
  return(matA);
}
  

```

```{r test6}
set.seed(121)
matA <- simmat(5, 4, 0, 1)
matA
# Is the simulation reproducible?
set.seed(121)
identical(matA, simmat(5, 4, 0, 1))
# You can also reproduce this matrix natively in R
set.seed(121)
identical(matA, matrix(rnorm(5*4, 0, 1), 5,4))
```


# Replicating simulations from within Eigen

```{Rcpp cppfun7}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::MatrixXd;
using Eigen::VectorXd;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

VectorXd simnorm(int nrow, int ncol, int B) {
  // Simulate B random matrices of dimension nrow x ncol
  // and return their square norms. The entries are drawn
  // from nrow*ncol independent N(0,1) variates
  
  // The return vector will contain the resulting N norms
  VectorXd out(B);
  // Each matrix will be of dimension nrow x ncol
  MatrixXd matA(nrow, ncol);
  
  double matnorm;
  // Note that vectors in C++ are typically zero-based
  // 0 .. B-1 instead of 1 .. B
  for(int b = 0; b < B; ++b) {
     matA = MatrixXd::Random(nrow , ncol);
	 out(b) = matA.norm();
  }
	 
  return(out);
}

```

```{r test7}
set.seed(121)
# Simulate B = 3  matrices of dimension 5x3 and get their square norms
simres <-  simnorm(10, 5, 3)
simres
# Check if simulation is reproducible
set.seed(121)
identical(simres, simnorm(10, 5, 3))
```

# Boostrapping a vector

```{Rcpp cppboot}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::VectorXd;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]

VectorXd bootvec(VectorXd vecx) {
	// This function samples the entries of vecx with replacement
	
	// Create an output vector equal to the lenth of vecx
	int nr = vecx.rows();
	VectorXd vecxboot(nr);
	
	// Simulate nr replicates uniformly over {0,..., nr-1} with replacement
	Rcpp::NumericVector bootindex(nr);
    bootindex=Rcpp::floor(Rcpp::runif(nr)*nr);

    // Index the output using the bootstrap indices
	// Eigen 3.4 introduces new slicing features to provide a more
	// elegant solution
	for(int i = 0; i < bootindex.size(); ++i){
		vecxboot[i] = vecx(bootindex[i]);
	}
	
	return(vecxboot);
}
```

```{r rboot}
# First use the function to draw uniformly from {1,2,...,20} with replacement
set.seed(121)
vecx <- 1:20
boot1 <- bootvec(vecx)
boot1
boot2 <- bootvec(vecx)
boot2

# Are the bootstrap replicates reproducible
set.seed(121)
identical(boot1, bootvec(vecx))
identical(boot2, bootvec(vecx))


# Now generate a noise vector from Normal(0,1)
set.seed(334)
vecx <- rnorm(10, 0, 1)
boot3 <- bootvec(vecx)
boot3

# Why is the bootstrap replicate not reproducible?
set.seed(334)
boot4 <- bootvec(vecx)
identical(boot3, boot4)



```


# An example of using the microbenchmark package

An RcppEigen function for adding the elements of a vector
```{Rcpp cppfun8}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::VectorXd;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]
double sumcpp1(VectorXd vecx) {
  double out = 0.0;
  for(int i = 0 ; i < vecx.size(); ++i) {
	out += vecx[i];
  }
  return(out);
}
```

Another RcppEigen function for adding the elements of a vector

```{Rcpp cppfun9}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::VectorXd;

//[[Rcpp::depends(RcppEigen)]]
//[[Rcpp::export]]
double sumcpp2(VectorXd vecx) {
  double out = vecx.sum();
  return(out);
}
```		
Next, we write two R functions for adding up the elements of a vector.


```{r sumR}
# Greatly inefficient version
sumR1 <- function(x) {
  out <- 0.0
  for(i in seq_along(x)) {
    out <- out + x[i]
  }
  return(out)
}
# vectorized  version
sumR2 <- function(x) {
  out <- sum(x)
  return(out)
}

```

```{r checkfuns}
set.seed(3121)
vecx <- rnorm(100000)
sumcpp1(vecx)
sumcpp1(vecx)
sumR1(vecx)
sumR2(vecx)

```


```{r benchmark}
# Benchmark each approach using 1000 replicates
microbenchmark(
  sumR1 = sumR1(vecx),
  sumR2 = sumR2(vecx),
  sumcpp1 = sumcpp1(vecx),
  sumcpp2 = sumcpp2(vecx),
  times = 1000
) -> bmark
# Tabulate results
bmark
# Visualize results
ggplot2::autoplot(bmark) + ggplot2::theme_bw()
```

# A customized benchmarking example

In the previous example, we compared the four approaches using 
a vector of length $n=100000$ and then used built in approaches
for summarizing the results. Next, we will 


```{r bmark2}
set.seed(1213)

# Time unit for benchmarking
timeunit <- "microseconds"

# Number of replicates used for be

foreach::foreach(n = c(1000L, 10000L, 100000L), .combine = rbind) %do% {

  # Simulate random vector
  vecx <- rnorm(n)
  # Conduct benchmark analysis
  microbenchmark(
    sumR1 = sumR1(vecx),
    sumR2 = sumR2(vecx),
    sumcpp1 = sumcpp1(vecx),
    sumcpp2 = sumcpp2(vecx),
    times = 1000L,
    unit = timeunit
  ) -> bmark
  # return results
  data.frame(n = n, as.data.frame(bmark))
} -> simres


# Graphical summary
simres %>%
  ggplot2::ggplot(aes(x = as.factor(n), y = time, col = expr)) +
  ggplot2::geom_boxplot() +
  ggplot2::theme_bw() +
  ggplot2::xlab("Size of vector (n)") +
  ggplot2::ylab(paste0("Time (", timeunit,")"))

simres %>%
  dplyr::group_by(n, expr) %>%
  summarize(
    B = dplyr::n(),
    mean = mean(time),
    median = median(time),
    max = max(time),
    min = min(time),
    IQR = IQR(time)
  ) %>%
  dplyr::arrange(n, expr) ->
  outtab

outtab

```

While the simulation of the vectors used in the benchmarking analysis
can be made reproducible, the timings cannot.

Finally, consider using the kableExtra 
(https://cran.r-project.org/web/packages/kableExtra/vignettes/awesome_table_in_html.html) 
package to customize table output

```{r kableextra}

outtab %>%
  kableExtra::kbl(caption = "Benchmark summary table") %>%
  kableExtra::kable_classic(full_width = FALSE, html_font = "Cambria")

```

# Sort a vector

```{Rcpp sortvec1}
#include <Rcpp.h>
#include <RcppEigen.h>

using Eigen::VectorXd;

//[[Rcpp::depends(RcppEigen)]]

//[[Rcpp::export]]
VectorXd sortvec(VectorXd y) {
  std::sort(y.data(), y.data()+y.size());
  return(y);
}
 
```

```{r s1}
set.seed(2131)
n <- 10
x <- rnorm(n)
data.frame(idx = seq_len(n), x = x , sortx = sortvec(x))

```




# Session Information

```{r sessinfo}
sessionInfo()

```

